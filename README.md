# World Wide Importers

## How to setup

### 0: Create a Config
First create a config file in ./config/Config.php and setup an array. Example:
```
<?php
return [
    'globals' => [
        'domain' => [
            'protocol' => 'https',
            'host' => 'dev.local',
            'port' => 8443
        ]
    ],
    'database' => [
        'host' => 'wwi_mysql',
        'name' => 'wwi',
        'username' => 'wwi',
        'password' => 'z2LfWP2E0p9af6k2lm5ExQcawZLuXTKC',
        'charset' => 'utf8'
    ],
    'mollie' => [
        'apiKey' => 'test_APIKEYHIER'
    ]
];
```

### 1: Docker Swarm
Then create a swarm on your host:
```bash
docker swarm init
```

### 2: Docker Login
Then login to the gitlab registry with your gitlab account:
```bash
docker login registry.gitlab.com
```

### 3: Configure Docker Compose
After that create a `docker-local.yml` file in the `./config` directory, and configure it. Example:
```yaml
version: '3.3'

services:
    mysql:
        environment:
            - MYSQL_ROOT_PASSWORD=<Your Root Password Here>
            - MYSQL_DATABASE=wwi
            - MYSQL_USER=wwi
            - MYSQL_PASSWORD=z2LfWP2E0p9af6k2lm5ExQcawZLuXTKC

    web:
        deploy:
            labels:
                - traefik.frontend.rule=Host:<Your Domain Here>
```

### 4: Let's run it
Now run the next command within the project root:
```bash
docker stack deploy -c config/docker-compose.yml -c config/docker-local.yml wwi
```

## How to build the Docker Image

### 1: Customize the Dockerfile to suit your needs
The Dockerfile contains the instructions on how to build our Docker image, configure it the way you want and everything within it will be persistent.

### 2: Building en tagging the Dockerfile`
Let's build the Dockerfile and tag it:

**Sidenote 1**: Replace `<version>` with the new version, see the [registry](https://gitlab.com/ictdt1a/world-wide-importers/container_registry) for the current version.

**Sidenote 2**: If you did change the version, don't forget to update the `./config/docker-compose.yml` and set the new version for the `web` service! 
```bash
docker build -t registry.gitlab.com/ictdt1a/world-wide-importers/wwi_php:<version> .
``` 

### 3: Pushing the docker image
Now that the image is build we can push it to the registry so each teammember has access to it:
```bash
docker push registry.gitlab.com/ictdt1a/world-wide-importers/wwi_php:<version>
```

## Database 

### 1: Setup database
`DO THIS ONLY WHEN THERE ISN'T DATA IN THE DATABASE OR THE DATABASE NEEDS TO UPDATE THE DATA`
1. Stop all running WWI containers in docker, and delete the `wwi_mysql-data` volume in docker, and if needed also all the services and containers for WWI. (Use [Portainer](https://portainer.readthedocs.io/en/latest/deployment.html) to manage al your docker containers.)
2. Now run the next command within the project root:
```bash
docker stack deploy -c config/docker-compose.yml -c config/docker-local.yml wwi
```
(keep in mind that it will take some minutes to import this SQL file. Thats why MYSQL and PHPMYADMIN do not immediately work after running the above command)