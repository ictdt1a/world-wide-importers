#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- apache2-foreground "$@"
fi

#cd /var/www/
#composer install
#cd /var/www/html/

exec "$@"