<?php
include_once '../lib/Renderer.php';

class Application
{
    /**
     * @var Renderer
     */
    protected $renderer;

    /**
     * @var Application
     */
    protected static $default;

    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @var array
     */
    protected static $globals = [];

    /**
     * Application constructor.
     */
    protected function __construct()
    {
        if (!isset(Application::$default)) {
            // Creating a default Renderer.
            $this->renderer = new Renderer(dirname(__FILE__) . '/../src/view');

            Application::$default = $this;
        }
    }

    /**
     * Simply bootstrapping the Application.
     *
     * @return Application
     */
    public static function bootstrap()
    {
        return new Application();
    }

    public function setDefaultMasterView($path)
    {
        Renderer::setMasterView($path);

        return $this;
    }

    /**
     * Load php files from a certain directory.
     *
     * @param $directory Directory
     * @return $this
     */
    public function load($directory)
    {
        foreach (new DirectoryIterator($directory) as $fileInfo) {
            if ($fileInfo->isDot()) continue;
            if ($fileInfo->isDir()) {
                $this->load($fileInfo->getPath() . '/' . $fileInfo->getFilename());
            } else {
                if (Helpers::endsWith($fileInfo->getFilename(), '.php')) {
                    include_once $fileInfo->getPath() . '/' . $fileInfo->getFilename();
                }
            }
        }

        return $this;
    }

    /**
     * Start the Application and his services.
     *
     * @return $this
     * @throws Exception
     */
    public function start()
    {
        session_start();

        if ($_SESSION['authenticated']) {
            static::setGlobal('user', PeopleModel::getOne([
                'LogonName' => $_SESSION['authenticated']
            ]));
        }

        // If the method does not exist, we won't have any routes for it by default.
        if (!array_key_exists($_SERVER['REQUEST_METHOD'], $this->routes)) {
            echo Renderer::fullRender(Renderer::render('404View'));
            return $this;
        }
        $path = $_GET['path'];

        // Let's fix up $path if needed.
        if (!Helpers::startsWith($path, '/')) {
            $path = '/' . $path;
        }
        if (!Helpers::endsWith($path, '/')) {
            $path .= '/';
        }

        foreach ($this->routes[$_SERVER['REQUEST_METHOD']] as $route => $data) {
            $handler = $data['handler'];
            $params = $data['params'];

            $foundParams = [];
            $foundRoute = $route;
            $fullRegexRoute = str_replace('/', '\/', $route);

            // Let's check if we have params, if so we should valid the URL using these params.
            if (!empty($params)) {
                $regexRoute = str_replace('/', '\/', $route);
                foreach ($params as $key => $regex) {
                    $regexTestRoute = str_replace(':' . $key . ':', '(' . $regex . ')', $regexRoute);
                    $regexTestRoute = preg_replace('/:.*?:/', '.*?', $regexTestRoute);

                    if (preg_match('/' . $regexTestRoute . '/', $path, $match)) {
                        $foundParams[$key] = $match[1];
                        $foundRoute = str_replace(':' . $key . ':', $match[1], $foundRoute);
                        $fullRegexRoute = str_replace(':' . $key . ':', '(' . $regex . ')', $fullRegexRoute);
                    }
                }
            }

            // HIER JURJEN 404
            // Let's do one final regex check and parameter count.
            if (preg_match('/' . $fullRegexRoute . '/', $path, $match) && count($params) === count($foundParams) && $foundRoute === $path) {
                // Now we have figured out the route, we can start calling the handler.
                $handler = explode('::', $handler);
                if (count($handler) >= 2) {
                    $class = $handler[0];
                    $method = $handler[1];

                    try {
                        $controller = new $class();
                        $data = Renderer::fullRender($controller->{$method}($foundParams));
                        $domainConfig = static::getGlobal('domain');
                        $domain = $domainConfig['protocol'] . "://" . $domainConfig['host'] . ":" . $domainConfig['port'];
                        echo preg_replace('/href="\/(.*?)"/', "href='" . $domain . "/$1'", $data);
                        return $this;
                    } catch (Exception $e) {
                        echo 'Caught exception: ', $e->getMessage();
                    }
                } else {
                    // throw exception with a good explanation.
                }
            }
        }

        echo Renderer::fullRender(Renderer::render('404View'));

        return $this;
    }

    public function get($route, $handler, $params = [])
    {
        return $this->internalRoute('GET', $route, $handler, $params);
    }

    public function post($route, $handler, $params = [])
    {
        return $this->internalRoute('POST', $route, $handler, $params);
    }

    public function put($route, $handler, $params = [])
    {
        return $this->internalRoute('PUT', $route, $handler, $params);
    }

    public function delete($route, $handler, $params = [])
    {
        return $this->internalRoute('DELETE', $route, $handler, $params);
    }

    protected function internalRoute($method, $route, $handler, $params)
    {
        if (!is_array($params)) {
            throw new Exception('$params should be an Array');
        }

        if (!Helpers::startsWith($route, '/')) {
            $route = '/' . $route;
        }
        if (!Helpers::endsWith($route, '/')) {
            $route .= '/';
        }

        if (!array_key_exists($method, $this->routes)) {
            $this->routes[$method] = [];
        }
        $this->routes[$method][$route] = [
            'handler' => $handler,
            'params' => $params
        ];

        return $this;
    }

    public static function setGlobal($key, $value)
    {
        Application::$globals[$key] = $value;
    }

    public static function getGlobal($key)
    {
        return Application::$globals[$key];
    }

    public static function setGlobals($globals)
    {
        Application::$globals = array_merge(Application::$globals, $globals);
    }

    public static function getGlobals()
    {
        return Application::$globals;
    }
}
