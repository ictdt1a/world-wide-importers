<?php

class Database
{

    protected static $instance;

    protected function __construct()
    {

    }

    /**
     * @return PDO connection object
     */
    public static function instance()
    {
        if (self::$instance === null) {
            $config = include '../config/Config.php';
            $config = $config['database'];

            $options = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES => FALSE,
            );

            $dataSource = 'mysql:host=' . $config['host'] . ';dbname=' . $config['name'] . ';charset=' . $config['charset'];
            self::$instance = new PDO($dataSource, $config['username'], $config['password'], $options);
        }
        return self::$instance;
    }

    /**
     * Returns the return value of the static callback
     *
     * @param $method
     * @param $args
     * @return mixed
     */
    public static function __callStatic($method, $args)
    {
        // Call the the instance() with arguments
        return call_user_func_array(
            array(self::instance(), $method), $args
        );
    }

    /**
     * @param string $sql SQL call
     * @param array $args
     * @return bool|false|PDOStatement
     */
    public static function run($sql, $args = [])
    {
        $stmt = self::instance()->prepare($sql);
        $stmt->execute($args);

        return $stmt;
    }

    public function lastInsertId()
    {
        return self::instance()->lastInsertId();
    }

}