<?php

class Helpers
{
    /**
     * Check if a string starts with a certain string.
     *
     * @param string $str String to search in.
     * @param string $search Search parameter.
     *
     * @return boolean  Returns if the string starts with the given parameter.
     */
    public static function startsWith($str, $search)
    {
        $length = strlen($search);
        return (substr($str, 0, $length) === $search);
    }

    /**
     * Check if a string ends with a certain string.
     *
     * @param string $str String to search in.
     * @param string $search Search parameter.
     *
     * @return boolean  Returns if the string ends with the given parameter.
     */
    public static function endsWith($str, $search)
    {
        $length = strlen($search);
        if ($length == 0) {
            return true;
        }

        return (substr($str, -$length) === $search);
    }
}