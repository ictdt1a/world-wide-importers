<?php
include_once '../lib/Renderer.php';

class Mailler
{
    /**
     * @param $to
     * @param $subject
     * @param $templatePath
     * @param array $params
     * @param array $headers
     * @return bool
     * @throws Exception
     */
    public static function mail($to, $subject, $templatePath, $params = [], $headers = [])
    {
        $realHeaders = [
            "From" => "noreply@wwi-shop.nl",
            "Reply-To" => "noreply@wwi-shop.nl",
            "MIME-Version" => "1.0",
            "Content-Type" => "text/html; charset=UTF-8"
        ];

        $headers = array_merge($realHeaders, $headers);
        $headerString = "";
        foreach ($headers as $key => $value) {
            $headerString .= $key . ": " . $value . "\r\n";
        }

        return mail($to, $subject, Renderer::render($templatePath, $params), $headerString);
    }
}