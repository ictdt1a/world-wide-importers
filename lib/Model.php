<?php
include_once '../lib/QueryBuilder.php';

class Model
{
    protected static $table;

    protected static $primary;

    protected static $validEnabled = true;

    public $validFrom;

    public $validTo;

    public $lastEditedBy;

    public function __construct($data)
    {
        if (empty(static::$primary)) {
            throw new Exception("The \$primary has not been implemented in the " . static::class);
        }
        foreach ($data as $key => $value) {
            if (property_exists($this, lcfirst($key))) {
                $this->{lcfirst($key)} = $value;
            }
        }
    }

    /**
     * Check if this Model is valid using `validFrom` and `validTo`;
     *
     * Returns true if neither of them are implemented.
     *
     * @return bool
     */
    public function isValid()
    {
        if (empty($this->validFrom) && empty($this->validTo)) {
            return true;
        }
        $now = strtotime("now");
        if (strtotime($this->validFrom) <= $now && $now <= strtotime($this->validTo)) {
            return true;
        }
        return false;
    }

    /**
     * Update and save the current Model to the database.
     *
     * @param $data
     * @throws ReflectionException
     */
    public function update($data)
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, lcfirst($key))) {
                $this->{lcfirst($key)} = $value;
            }
        }
        $this->save();
    }

    /**
     * Save the current Model to the database.
     *
     * @return $this
     * @throws ReflectionException
     */
    public function save()
    {
        $props = self::getProperties();

        $set = [];
        foreach ($props as $prop) {
            if ($prop === static::$primary) {
                continue;
            }
            $set[] = ucfirst($prop) . " = " . Model::properValue($this->{$prop});
        }

        $query = "UPDATE " . static::$table . " SET " . implode($set, ', ') . " WHERE " . static::$primary . " = " . $this->{static::$primary};
        Database::run($query);

        return $this;
    }

    /**
     * @param array $data The main data used by the Model
     * @param array $restData Rest data that the Model does not use
     * @return Model
     * @throws Exception
     */
    public static function insert($data, $restData = [])
    {
        if (empty(static::$table)) {
            throw new Exception("The \$table has not been implemented in the " . static::class);
        }

        $props = self::getProperties();

        $colums = [];
        $values = [];
        $keys = [];
        foreach ($props as $prop) {
            if (!array_key_exists($prop, $data) && $prop !== static::$primary) {
                switch ($prop) {
                    case 'validFrom':
                        if (static::$validEnabled) {
                            $data[$prop] = date("Y-m-d H:i:s", strtotime("now"));
                        }
                        break;
                    case 'validTo':
                        if (static::$validEnabled) {
                            $data[$prop] = "9999-12-31 23:59:59";
                        }
                        break;
                    case 'lastEditedBy':
                        $data[$prop] = 1;
                        break;
                    default:
                        if ($prop !== lcfirst(static::$primary)) {
                            throw new Exception("Missing '$prop', in " . static::class . "::insert");
                        }
                        break;
                }
            }
            $key = ":" . $prop;
            switch ($prop) {
                case 'validFrom':
                case 'validTo':
                    if (static::$validEnabled) {
                        $colums[] = ucfirst($prop);
                        $values[$key] = $data[$prop];
                        $keys[] = $key;
                    }
                    break;
                default:
                    if ($prop !== lcfirst(static::$primary)) {
                        $colums[] = ucfirst($prop);
                        $values[$key] = $data[$prop];
                        $keys[] = $key;
                    }
                    break;
            }
        }
        foreach ($restData as $prop => $value) {
            $key = ":" . $prop;
            $colums[] = ucfirst($prop);
            $values[$key] = $value;
            $keys[] = $key;
        }

        $query = "INSERT INTO " . static::$table . " (" . implode($colums, ", ") . ") VALUES (" . implode($keys, ", ") . ")";

        Database::run($query, $values);
        $result = Database::lastInsertId();

        return self::getById($result);
    }

    /**
     * @param array $params The where parameters
     */
    public static function delete($params)
    {
        $where = [];
        foreach ($params as $key => $value) {
            $where[] = "$key = " . Model::properValue($value);
        }
        $query = "DELETE FROM " . static::$table . (!empty($where) ? " WHERE " . implode($where, " AND ") : '');
        Database::run($query);
    }

    /**
     * @param array $params
     * @throws Exception
     *
     * @return static[]
     */
    public static function get($params = [])
    {
        if (empty(static::$table)) {
            throw new Exception("The \$table has not been implemented in the " . static::class);
        }
        try {
            $props = self::getProperties();

            $select = [];
            foreach ($props as $prop) {
                switch ($prop) {
                    case 'validFrom':
                    case 'validTo':
                        if (static::$validEnabled) {
                            $select[] = static::$table . "." . ucfirst($prop);
                        }
                        break;
                    default:
                        $select[] = static::$table . "." . ucfirst($prop);
                        break;
                }
            }

            $where = null;
            if (!empty($params)) {
                foreach ($params as $key => $value) {
                    $where[] = [ucfirst($key), "=", $value];
                }
            }

            $qb = new QueryBuilder();
            $qb->select(implode($select, ', '))
                ->from(static::$table);

            if (!empty($where)) {
                $qb->where($where);
            }

            $results = $qb->retrieve();
            $models = [];

            $class = static::class;
            foreach ($results as $result) {
                $models[] = new $class($result);
            }

            return $models;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Get a Model by its Primary ID.
     *
     * @param int $id
     * @throws Exception
     * @return Model
     */
    public static function getById($id)
    {
        $primary = static::$primary;

        return self::getOne([
            $primary => $id
        ]);
    }

    /**
     * Get one Model with where parameters
     *
     * @param array $params
     * @return Model
     * @throws Exception
     */
    public static function getOne($params = [])
    {
        return self::get($params)[0];
    }

    /**
     * Return the property names of the Model.
     *
     * @return array
     * @throws ReflectionException
     */
    private static function getProperties()
    {
        $reflect = new ReflectionClass(static::class);
        $props = $reflect->getProperties(ReflectionProperty::IS_PUBLIC);

        $returns = [];
        foreach ($props as $prop) {
            $returns[] = $prop->getName();
        }

        return $returns;
    }
}