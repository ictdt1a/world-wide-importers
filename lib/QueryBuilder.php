<?php
include_once '../lib/Database.php';

class QueryBuilder
{
    protected $_select = [];

    protected $_from;

    protected $_where = [];

    protected $_orWhere = [];

    protected $_join = [];

    protected $_limit;

    public function select($select)
    {
        if (is_string($select)) {
            $select = [$select];
        }
        $this->_select = array_merge($this->_select, $select);

        return $this;
    }

    public function where($wheres)
    {
        if (!is_array($wheres[0])) {
            $wheres = [$wheres];
        }
        foreach ($wheres as $where) {
            $key = $where[0];
            $ops = $where[1];
            $val = $where[2];

            $this->_where[] = [
                "top" => "AND",
                "key" => $key,
                "ops" => $ops,
                "val" => $val,
            ];
        }

        return $this;
    }

    public function orWhere($wheres)
    {
        if (!is_array($wheres[0])) {
            $wheres = [$wheres];
        }
        foreach ($wheres as $where) {
            $key = $where[0];
            $ops = $where[1];
            $val = $where[2];

            $this->_where[] = [
                "top" => "OR",
                "key" => $key,
                "ops" => $ops,
                "val" => $val,
            ];
        }

        return $this;
    }

    public function from($table, $as = null)
    {
        $this->_from = "$table" . (!empty($as) ? " AS $as" : "");

        return $this;
    }

    public function join($table, $as, $on = null)
    {
        if (is_null($on)) {
            $on = $as;
            $as = null;
        }
        $this->_join[] = "JOIN $table " . (!is_null($as) ? "AS $as " : "") . "ON $on";

        return $this;
    }

    public function leftJoin($table, $as, $on = null)
    {
        if (is_null($on)) {
            $on = $as;
            $as = null;
        }
        $this->_join[] = "LEFT JOIN $table " . (!is_null($as) ? "AS $as " : "") . "ON $on";

        return $this;
    }

    public function rightJoin($table, $as, $on = null)
    {
        if (is_null($on)) {
            $on = $as;
            $as = null;
        }
        $this->_join[] = "RIGHT JOIN $table " . (!is_null($as) ? "AS $as " : "") . "ON $on";

        return $this;
    }

    /**
     * @param int $limit
     */
    public function limit($limit)
    {
        $this->_limit = "LIMIT $limit";
    }

    public function retrieve()
    {
        $select = implode($this->_select, ', ');
        $from = $this->_from;
        $join = implode($this->_join, ' ');

        $wheres = '';
        $wheresValues = [];
        foreach ($this->_where as $where) {
            $key = str_replace('.', '', ":" . $where["key"]);
            $wheres .= ($where === '' ? $where["top"] : '') . $where["key"] . " " . $where["ops"] . " " . $key;
            $wheresValues[$key] = $where["val"];
        }
        if ($wheres != '') {
            $wheres = "WHERE " . $wheres . " ";
        }

        $limit = $this->_limit;

        $query = "SELECT $select FROM $from " . " $join " . $wheres . ($limit ? $limit : "");

        $stmt = Database::run($query, $wheresValues);
        return $stmt->fetchAll();
    }

    public static function build()
    {
        $qb = new self();
        return $qb;
    }
}