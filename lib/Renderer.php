<?php
include_once '../lib/Helpers.php';
include_once '../lib/Application.php';

class Renderer
{
    /**
     * Default renderer
     *
     * @var Renderer
     */
    protected static $default;

    /**
     * @var string
     */
    protected static $masterView;

    /**
     * View directory for a Renderer instance.
     *
     * @var string
     */
    public $directory;

    /**
     * @var string
     */
    protected $title = "WWI";

    /**
     * Construct a new Renderer for a specific directory.
     *
     * @param string $directory The directory where to search in.
     */
    public function __construct($directory)
    {
        if (!Helpers::endsWith($directory, '/')) {
            $directory .= '/';
        }
        $this->directory = $directory;

        if (!isset(Renderer::$default)) {
            Renderer::$default = $this;
        }
    }

    /**
     * Set the title for the view.
     *
     * @param string $title The title to set
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Simply render the file with the given data.
     *
     * @param string $filePath Path to the file.
     * @param mixed[] $data Array of data that is used for rendering.
     * @throws Exception Throw an Exception if given $filePath does not exist.
     *
     * @return string Return the rendered file.
     */
    public function renderFile($filePath, $data = [])
    {
        // Enforcing an extension!
        $pathInfo = pathinfo($filePath);
        if ($pathInfo['extension'] === NULL) {
            $filePath .= '.php';
        }

        if (!file_exists($this->directory . $filePath)) {
            throw new Exception('File not found: ' . $this->directory . $filePath);
        }

        foreach (Application::getGlobals() as $key => $value) {
            ${$key} = $value;
        }

        // Let's turn on output buffering
        ob_start();
        // Including the file, because of the block scope of this function, the file will have access to the $data parameter.
        include $this->directory . $filePath;
        // Get the output buffer and delete it.
        return ob_get_clean();
    }

    /**
     * Quick rendering using the default Renderer.
     *
     * @param string $filePath Path to the file.
     * @param mixed[] $data Array of data that is used for rendering.
     * @throws Exception Throw an Exception if given $filePath does not exist.
     *
     * @example
     * Renderer::render('ExampleView', [
     *      'something' => 'World!'
     * ]);
     *
     * @return string   Return the rendered file.
     */
    public static function render($filePath, $data = [])
    {
        $renderer = Renderer::$default;
        if (!isset($renderer)) {
            throw new Exception('No default Renderer found');
        }
        return $renderer->renderFile($filePath, $data);
    }

    public static function fullRender($data = [])
    {
        $renderer = Renderer::$default;
        if (!isset($renderer)) {
            throw new Exception('No default Renderer found');
        }
        if (!isset(Renderer::$masterView)) {
            throw new Exception('No default MasterView, please set one');
        }

        return $renderer->renderFile(Renderer::$masterView, $data);
    }

    public static function notification($status, $title, $message)
    {
        return $_SESSION['notification'] = ["status" => $status, "title" => $title, "message" => $message];
    }


    /**
     * @param string $path Path to the MasterView
     */
    public static function setMasterView($path)
    {
        Renderer::$masterView = $path;
    }
}