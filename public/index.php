<?php
require '../vendor/autoload.php';
include_once '../lib/Application.php';

//TODO find alternive location for next shizzel
session_start();


// Initialize app.
$app = Application::bootstrap();

// Set default MasterView
$app->setDefaultMasterView('./templates/MasterView');

// Load controllers.
$app->load(__DIR__ . '/../src/controller');

// Add routes.
$app->get('/example/:variableExample:', 'ExampleController::exampleAction', ['variableExample' => '\w+']);

$app->get('/registreren', 'RegistrationController::registerView');
$app->post('/registreren', 'RegistrationController::saveUser');

$app->get('/login', 'LoginController::loginView');
$app->post('/login', 'LoginController::login');
$app->get('/loguit', 'LoginController::logout');

$app->get('/profiel', 'UserController::profile');

$app->get('/', 'MainController::homeIndex');

$app->get('/winkelwagen', 'CartController::cartIndex');
$app->get('/winkelwagen/clear', 'CartController::clearCart');
$app->get('/winkelwagen/add/:productId:', 'CartController::addProductToCart', ['productId' => '\w+']);
$app->get('/winkelwagen/minus/:productId:', 'CartController::minusProductToCart', ['productId' => '\w+']);
$app->get('/winkelwagen/delete/:productId:', 'CartController::deleteProductFromCart', ['productId' => '\w+']);

$app->get('/bestellen', 'CartController::order');
$app->post('/bestellen', 'CartController::goOrder');
$app->get('/bestellen/:orderId:/voltooid', 'CartController::orderCompleted', ['orderId' => '\w+']);

$app->get('/product/:productId:', 'ProductController::productDetail', ['productId' => '\w+']);
$app->get('/producten', 'ProductController::products');
$app->get('/producten/categorie/:categoryId:', 'ProductController::category', ['categoryId' => '\w+']);

$app->get('/zoeken', 'ProductController::searchProduct');
$app->post('/zoeken', 'ProductController::searchProduct');


// Setting globals
$config = include '../config/Config.php';
Application::setGlobals($config['globals']);

// Start the app
$app->start();