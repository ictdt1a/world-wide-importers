<?php
include_once '../lib/Renderer.php';
include_once '../lib/Mailler.php';
include_once '../lib/QueryBuilder.php';
include_once '../src/model/StockItemModel.php';
include_once '../src/model/CartModel.php';
include_once '../src/model/OrderModel.php';
include_once '../src/model/OrderLineModel.php';
include_once '../src/model/InvoiceModel.php';
include_once '../src/model/InvoiceLineModel.php';

class CartController
{
    protected static $taxRate = 21;

    /**
     * @return string
     * @throws Exception
     */
    public function cartIndex()
    {
        $products = [];

        if (!(empty($_SESSION['cart']))) {
            $total = 0;
            $totalBtw = 0;

            foreach ($_SESSION['cart'] as $key => $cartItem) {
                $product = [];
                $product[] = $productObject = StockItemModel::getById($key);
                $product['amount'] = $cartItem['amount'];
                $products[] = $product;
                $total += $product[0]->recommendedRetailPrice * $product['amount'];
                $totalBtw += $product[0]->getPriceWithTax() * $product['amount'];
            }

            return Renderer::render('CartView', [
                'products' => $products,
                'total' => $total,
                'totalBtw' => $totalBtw
            ]);

        } else {
            Renderer::notification('general', 'Geen producten in winkelwagen', 'Er bevinden momenteel nog geen producten in de winkelwagen, voeg een product toe.');
        }
    }

    /**
     * @param $params
     * @throws Exception
     */
    public function addProductToCart($params)
    {
        $product = StockItemModel::getById($productId = $params['productId']);;

        if (!(is_null($product))) {
            //checks if session cart is empty
            if (empty ($_SESSION['cart'])) {
                $_SESSION['cart'] = [];
            }
            // adds product to session: cart
            if (!(CartModel::addIfProductExistsInCart($productId))) {
                $_SESSION['cart'][$productId] = ["amount" => 1];
            }

            header("Location: /winkelwagen");
            die();
        } else {
            Renderer::notification('danger', 'Product niet toegevoegd aan winkelwagen', 'Het product bestaat niet meer of is niet meer geldig.');
        }
    }

    /**
     * @param $params
     * @throws Exception
     */
    public function minusProductToCart($params)
    {
        $product = StockItemModel::getById($productId = $params['productId']);;

        if (!(is_null($product))) {
            //checks if session cart is empty
            if (empty ($_SESSION['cart'])) {
                $_SESSION['cart'] = [];
            }
            // adds product to session: cart
            if (!(CartModel::minusIfProductExistsInCart($productId))) {
                $_SESSION['cart'][$productId] = ["amount" => 1];
            }

            header("Location: /winkelwagen");
            die();
        } else {
            Renderer::notification('danger', 'Product niet uit winkelwagen gehaald.', 'Het product bestaat niet meer of is niet meer geldig.');
        }
    }

    /**
     * @param $params
     * @return string
     * @throws Exception
     */
    public function deleteProductFromCart($params)
    {
        $productId = $params['productId'];

        foreach ($_SESSION['cart'] as $key => $product) {
            if ($key == $productId) {
                unset($_SESSION['cart'][$productId]);
            }
        }

        header("Location: /winkelwagen");
        die();
    }

    /**
     * @return string
     * @throws Exception
     */
    public function order()
    {
        if (!isset($_SESSION['authenticated'])) {
            Renderer::notification('danger', 'U moet eerst inloggen', 'Om uw bestelling te voltooien moet u eerst inloggen met uw account.');
            return;
        }

        $products = [];
        if (!(empty($_SESSION['cart']))) {
            $total = 0;
            $totalBtw = 0;

            foreach ($_SESSION['cart'] as $key => $cartItem) {
                $product = [];
                $product[] = $productObject = StockItemModel::getById($key);
                $product['amount'] = $cartItem['amount'];
                $products[] = $product;
                $total += $product[0]->recommendedRetailPrice * $product['amount'];
                $totalBtw += $product[0]->getPriceWithTax() * $product['amount'];
            }

            return Renderer::render('OrderView', [
                'products' => $products,
                'total' => $total,
                'totalBtw' => $totalBtw
            ]);
        } else {
            Renderer::notification('general', 'Geen producten om te bestellen', 'Er bevinden momenteel nog geen producten in de winkelwagen, voeg een product toe.');
        }
    }

    /**
     * @throws Exception
     */
    public function goOrder()
    {
        if (!isset($_SESSION['authenticated'])) {
            Renderer::notification('danger', 'U moet eerst inloggen', 'Om uw bestelling te voltooien moet u eerst inloggen met uw account.');
            return;
        }
        $user = Application::getGlobal('user');

        if (!(empty($_SESSION['cart']))) {
            $orderModel = OrderModel::insert([
                'customerID' => 1,
                'salespersonPersonID' => 1,
                'contactPersonID' => $user->personID,
                'orderDate' => date("Y-m-d H:i:s", strtotime("now")),
                'expectedDeliveryDate' => date("Y-m-d H:i:s", strtotime("now")),
                'isUndersupplyBackordered' => 1,
                'lastEditedBy' => 1,
                'lastEditedWhen' => date("Y-m-d H:i:s", strtotime("now"))
            ]);

            $total = 0;
            $totalBtw = 0;
            foreach ($_SESSION['cart'] as $key => $cartItem) {
                $stockItem = StockItemModel::getById($key);
                $total += $stockItem->recommendedRetailPrice * $cartItem['amount'];
                $totalBtw += $stockItem->getPriceWithTax() * $cartItem['amount'];

                $orderLineModel = OrderLineModel::insert([
                    'orderID' => $orderModel->orderID,
                    'stockItemID' => $stockItem->stockItemID,
                    'description' => $stockItem->marketingComments,
                    'packageTypeID' => $stockItem->outerPackageID,
                    'quantity' => $cartItem['amount'],
                    'unitPrice' => $stockItem->unitPrice,
                    'taxRate' => $stockItem->taxRate,
                    'pickedQuantity' => $cartItem['amount'],
                    'pickingCompletedWhen' => date("Y-m-d H:i:s", strtotime("now")),
                    'lastEditedBy' => 1,
                    'lastEditedWhen' => date("Y-m-d H:i:s", strtotime("now"))
                ]);
            }

            $domainConfig = Application::getGlobal('domain');
            $config = include '../config/Config.php';
            $config = $config['mollie'];

            $mollie = new \Mollie\Api\MollieApiClient();
            $mollie->setApiKey($config['apiKey']);

            $payment = $mollie->payments->create([
                "amount" => [
                    "currency" => "EUR",
                    "value" =>  money_format('%.2n', $total )
                ],
                "method" => \Mollie\Api\Types\PaymentMethod::IDEAL,
                "description" => "Bestelling #" . $orderModel->orderID,
                "redirectUrl" => $domain = $domainConfig['protocol'] . "://" . $domainConfig['host'] . ":" . $domainConfig['port'] . "/bestellen/" . $orderModel->orderID . "/voltooid",
                "metadata" => [
                    "orderId" => $orderModel->orderID
                ]
            ]);
            header("Location: " . $payment->getCheckoutUrl(), true, 303);
        } else {
            Renderer::notification('general', 'Geen producten om te bestellen', 'Er bevinden momenteel nog geen producten in de winkelwagen, voeg een product toe.');
        }
    }

    /**
     * @param $params
     * @return string
     * @throws Exception
     */
    public function orderCompleted($params)
    {
        $order = OrderModel::getById($params['orderId']);
        $user = Application::getGlobal('user');

        if ($order) {
            $orderLines = OrderLineModel::getByOrderID($order->orderID);

            $products = [];
            $totalChillerItems = 0;
            $totalDryItems = 0;
            foreach ($orderLines as $orderLine) {
                $product = [];
                $product[] = StockItemModel::getById($orderLine->stockItemID);
                $product['amount'] = $orderLine->quantity;
                if ($product[0]->isChillerStock) {
                    $totalChillerItems++;
                } else {
                    $totalDryItems++;
                }
                $products[] = $product;
            }

            $invoice = InvoiceModel::insert([
                'customerID' => 1,
                'billToCustomerID' => 1,
                'orderID' => $order->orderID,
                'deliveryMethodID' => 3,
                'contactPersonID' => $user->personID,
                'accountsPersonID' => 1,
                'salespersonPersonID' => 1,
                'packedByPersonID' => 1,
                'invoiceDate' => date("Y-m-d H:i:s", strtotime("now")),
                'isCreditNote' => 0,
                'totalDryItems' => $totalDryItems,
                'totalChillerItems' => $totalChillerItems,
                'lastEditedBy' => 1,
                'lastEditedWhen' => date("Y-m-d H:i:s", strtotime("now"))
            ]);

            $invoiceLines = [];
            foreach ($products as $product) {
                $invoiceLines[] = InvoiceLineModel::insert([
                    'invoiceID' => $invoice->invoiceID,
                    'stockItemID' => $product[0]->stockItemID,
                    'description' => $product[0]->marketingComments,
                    'packageTypeID' => $product[0]->outerPackageID,
                    'quantity' => $product['amount'],
                    'unitPrice' => $product[0]->unitPrice,
                    'taxRate' => $product[0]->taxRate,
                    'taxAmount' => $product[0]->getPriceWithTax() / $product[0]->taxRate,
                    'lineProfit' => 0,
                    'extendedPrice' => money_format('%.2n', $product[0]->getPriceWithTax() * $product['amount']),
                    'lastEditedBy' => 1,
                    'lastEditedWhen' => date("Y-m-d H:i:s", strtotime("now"))
                ]);
            }

            Mailler::mail($user->emailAddress, "Bestelling #" . $order->orderID, './templates/mail/InvoiceMail', [
                'order' => $order,
                'invoice' => $invoice,
                'products' => $products
            ]);

            $_SESSION['cart'] = NULL;
            return Renderer::render('OrderCompletedView', [
                'order' => $order,
                'invoice' => $invoice
            ]);
        } else {
            header("Location: /");
            die();
        }
    }

    /**
     * @return string
     */
    public function clearCart()
    {
        //unset session cart
        $_SESSION['cart'] = NULL;

        header("Location: /winkelwagen");
        die();
    }
}