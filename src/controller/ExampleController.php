<?php
include_once '../lib/Renderer.php';
include_once '../lib/QueryBuilder.php';
include_once '../src/model/PeopleModel.php';

class ExampleController
{
    public function __construct()
    {

    }

    public function exampleAction($params)
    {
//        $peoples = PeopleModel::build()
//                             ->select('p.FullName')
//                             ->from('people', 'p');

//        $person = PeopleModel::getOne([
//            'PersonID' => 2
//        ]);
//        $person->fullName = 'Test';
//        var_dump($person->save());

//        $person = PeopleModel::getById(2);
//        $person->update([
//            'fullName' => 'Test'
//        ]);

//        $person = PeopleModel::insert([
//            "fullName" => "Test 1",
//            "preferredName" => "Test",
//            "searchName" => "Test",
//            "isPermittedToLogon" => 1,
//            'isExternalLogonProvider' => 0,
//            'isSystemUser' => 0,
//            'isEmployee' => 0,
//            'isSalesPerson' => 0
//        ]);
//        var_dump($person->isValid());

//        $person = PeopleModel::delete([
//            "personID" => 92679
//        ]);

//        $queryBuilder = new QueryBuilder();
//        $queryBuilder->select('c.*')
//                     ->from('stockitems', 'c')
//                     ->join('suppliers', 's', 's.SupplierID = c.SupplierID')
//                     ->where('c.StockItemId = 1');
//
//        var_dump($queryBuilder->retrieve());

        return Renderer::render('ExampleView', [
            'world' => 'x',
            'items' => [
                ['title' => 'Title']
            ]
        ]);
    }
}