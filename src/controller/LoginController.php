<?php
include_once "../src/model/PeopleModel.php";
include_once '../lib/Renderer.php';
include_once '../lib/Model.php';

class LoginController
{
    /**
     * @return string
     * @throws Exception
     */
    public function login()
    {
        if ($_SESSION['authenticated']) {
            header("Location: /");
            die();
        }

        $email = $_POST['email'];
        $password = $_POST['password'];

        $errors = [];
        if (empty($email)) {
            $errors['email'] = '(ontbreekt)';
        }
        if (empty($password)) {
            $errors['password'] = '(ontbreekt)';
        }
        if (count($errors) >= 1) {
            return $this->loginView($errors);
        }

        if (PeopleModel::validatePassword($email, $password)) {
            $_SESSION['authenticated'] = $email;
            header("Location: /");
            die();
        }
        $errors['error'] = "Foutief e-mail/wachtwoord, probeer het opnieuw";
        return $this->loginView($errors);
    }

    /**
     * @param array $errors
     * @return string
     * @throws Exception
     */
    public function loginView($errors = [])
    {
        if ($_SESSION['authenticated']) {
            header("Location: /");
            die();
        }

        return Renderer::render('LoginView', [
            'errors' => $errors
        ]);
    }

    /**
     *
     */
    public function logout()
    {
        if ($_SESSION['authenticated']) {
            unset($_SESSION['authenticated']);
        }
        header("Location: /login");
        die();
    }

}
