<?php
include_once '../lib/Renderer.php';


class MainController
{
    public function __construct()
    {

    }

    public function homeIndex()
    {
        $queryBuilder = new QueryBuilder();
        $queryBuilder->select('c.*, m.MediaTypeID, m.Location')
            ->from('stockitems', 'c')
            ->leftJoin('media', 'm', 'm.StockItemID = c.StockItemID AND m.MediaTypeID = 1 ')
            ->limit(8);

        $products = $queryBuilder->retrieve();

        return Renderer::render('HomeView', [
            'products' => $products
        ]);
    }
}