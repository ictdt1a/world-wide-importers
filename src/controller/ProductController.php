<?php
include_once '../lib/Renderer.php';
include_once '../lib/QueryBuilder.php';
include_once '../src/model/StockItemModel.php';
include_once '../src/model/CategoryModel.php';
include_once '../src/model/MediaModel.php';

class ProductController
{
    /**
     * @param $params
     * @return string
     * @throws Exception
     */
    public function productDetail( $params )
    {
        $queryBuilder = new QueryBuilder();
        $queryBuilder->select('s.*, c.ColorName, p.PackageTypeName')
            ->from('stockitems', 's')
            ->leftJoin('colors', 'c', 'c.ColorID = s.ColorID')
            ->leftJoin('packagetypes', 'p', 'p.PackageTypeID = s.UnitPackageID')
            ->where(["s.StockItemID", "=", $params['productId']]);

        $product = $queryBuilder->retrieve();


        $queryBuilder2 = new QueryBuilder();
        $queryBuilder2->select('m.*')
            ->from('media', 'm')
            ->where(["m.StockItemID", "=", $params['productId']]);

        $media = $queryBuilder2->retrieve();

        if ( !( empty ( $product ) ) ) {
            return Renderer::render('ProductDetailView', [
                'product'   => $product[0],
                'media'     => $media
            ]);
        } else {
            Renderer::notification('warning', 'Geen product gevonden', 'Het product dat u zocht bestaat niet meer of is niet geldig. Ga een pagina terug');
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function searchProduct() {
        if ( isset( $_POST['search'] ) ) {

            $searchTerm = filter_var($_POST['searchTerm']);

            if ( !empty( $searchTerm ) ) {
                $queryBuilder = new QueryBuilder();
                $queryBuilder->select('c.*, m.MediaTypeID, m.Location')
                    ->from('stockitems', 'c')
                    ->leftJoin('media', 'm', 'm.StockItemID = c.StockItemID AND m.MediaTypeID = 1 ')
                    ->where(["c.SearchDetails", "LIKE", "%$searchTerm%"]);

                $products = $queryBuilder->retrieve();

                if (!(empty($products))) {
                    return Renderer::render('ProductsView', [
                        'searchTerm' => $searchTerm,
                        'products' => $products
                    ]);
                } else {
                    Renderer::notification('warning', 'Geen product(en) gevonden op: ' . $searchTerm, 'Probeer een ander trefwoord');
                }
            } else {
                Renderer::notification('warning', 'Geen zoekterm opgegeven', 'Voer een trefwoord in het zoekveld.');
            }
        } else {
            Renderer::notification('warning', 'Geen zoekterm opgegeven', 'Voer een trefwoord in het zoekveld.');
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function products()
    {
        return Renderer::render('ProductsView');
    }

    /**
     * @param $params
     * @return string
     * @throws Exception
     */
    public function category( $params )
    {
        $queryBuilder = new QueryBuilder();
        $queryBuilder->select('i.StockItemID, i.StockItemName, i.MarketingComments, i.RecommendedRetailPrice, i.TaxRate, g.StockGroupName, m.MediaTypeID, m.Location')
            ->from('stockitemstockgroups', 's')
            ->join('stockgroups', 'g', 's.StockGroupID = g.StockGroupID')
            ->join('stockitems', 'i', 's.StockItemID = i.StockItemID')
            ->leftJoin('media', 'm', 'm.StockItemID = i.StockItemID AND m.MediaTypeID = 1 ')
            ->where(['s.StockGroupID', "=", $params['categoryId']]);

        $products = $queryBuilder->retrieve();

        $category = CategoryModel::getById( $params['categoryId'] );

        if ( !( empty( $products ) ) ) {
            return Renderer::render('ProductsView', [
                'products' => $products,
                'category' => $category
            ]);
        } else {
            Renderer::notification('general', 'Geen producten bij deze categorie gevonden', 'Categorie bestaat waarschijnlijk niet of is niet van toepassing.');

        }
    }
}