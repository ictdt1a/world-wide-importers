<?php
include_once "../src/model/PeopleModel.php";
include_once '../lib/Renderer.php';
include_once '../lib/Model.php';

class RegistrationController
{
    /**
     * @return string
     * @throws Exception
     */
    public function saveUser()
    {
        if ($_SESSION['authenticated']) {
            header("Location: /");
            die();
        }
        $errors = [];

        if("" === $_POST['email'] || "" === $_POST['password'] || "" === $_POST['firstname'] ||
            "" === $_POST['lastname'] || "" === $_POST['phonenumber']) {
            $errors['generic'] = "Één of meerdere velden zijn niet ingevuld.";
            return $this->registerView($errors);
        }
        if ($_POST['password'] === $_POST['r_password']) {

            $email = $_POST['email'];
            $password = $_POST['password'];
            $firstname = $_POST['firstname'];
            $middlename = $_POST['middlename'];
            $lastname = $_POST['lastname'];
            $phonenumber = $_POST['phonenumber'];

            try {
                $person = PeopleModel::getByEmail($email);
                if ($person) {
                    $errors['generic'] = "E-mail '$email' is al reeds geregistreerd";
                } else {
                    $person = PeopleModel::insert([
                        "fullName" => "$firstname $middlename $lastname",
                        "preferredName" => $firstname,
                        "searchName" => "$firstname $firstname $middlename $lastname",
                        "isPermittedToLogon" => 1,
                        'logonName' => $email,
                        'isExternalLogonProvider' => 0,
                        'isSystemUser' => 0,
                        'isEmployee' => 0,
                        'isSalesPerson' => 0,
                        'userPreferences' => "{}",
                        'phoneNumber' => $phonenumber,
                        'faxNumber' => $phonenumber,
                        'emailAddress' => $email,
                        'photo' => "",
                        'customFields' => "{}",
                        'otherLanguages' => "{}"
                    ], [
                        "HashedPassword" => password_hash($password, PASSWORD_BCRYPT)
                    ]);

                    if ($person) {
                        Mailler::mail($person->emailAddress, "Welkom bij Wide World Importers", './templates/mail/RegistrationMail', [
                            'person' => $person
                        ]);

                        return $this->registerView($errors, true);
                    } else {
                        throw new Exception('Quick way out');
                    }
                }
            } catch (Exception $e) {
                $errors['generic'] = "Er is iets fout gegaan bij het registreren, probeer het opnieuw";
            }
        } else {
            $errors['mismatch'] = "Wachtwoorden komen niet overeen";
        }

        return $this->registerView($errors);
    }

    /**
     * @param array $errors
     * @return string
     * @throws Exception
     */
    public function registerView($errors = [], $done = false)
    {
        if ($_SESSION['authenticated']) {
            header("Location: /");
            die();
        }

        return Renderer::render('RegistrationView', [
            'errors' => $errors,
            'done' => $done
        ]);
    }
}
