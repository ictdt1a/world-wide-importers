<?php
include_once '../lib/Renderer.php';

class UserController
{
    /**
     * @return string
     * @throws Exception
     */
    public function Profile()
    {
        return Renderer::render('ProfileView');
    }
}
