<?php
class CartModel
{
    /**
     * @param $value
     * @return bool
     */
    public function addIfProductExistsInCart($value)
    {
        foreach ($_SESSION['cart'] as $key => $product) {
            if ($key == $value) {
                if ($_SESSION['cart'][$key]['amount'] <= 9) {
                    $_SESSION['cart'][$key]['amount']++;
                    return true;
                } else {
                    Renderer::notification('danger', 'Limit aantal van product overschreden', 'Maximaal aantal items van het product is overschreden');
                }
            }
        }
        return false;
    }

    /**
     * @param $value
     * @return bool
     */
    public function minusIfProductExistsInCart($value)
    {
        foreach ($_SESSION['cart'] as $key => $product) {
            if ($key == $value) {
                if ($_SESSION['cart'][$key]['amount'] >= 2) {
                    $_SESSION['cart'][$key]['amount']--;
                    return true;
                } else {
                    Renderer::notification('danger', 'Min aantal is 1', 'Minimaal 1 items van het product kan toegevoegd worden.');
                }
            }
        }
        return false;
    }

}