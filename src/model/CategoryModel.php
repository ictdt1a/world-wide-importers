<?php
include_once '../lib/Model.php';

class CategoryModel extends Model
{
    protected static $table = 'stockgroups';

    protected static $primary = 'StockGroupID';

    public $stockGroupID;

    public $stockGroupName;

}