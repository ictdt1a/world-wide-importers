<?php
include_once '../lib/Model.php';

class DeliveryMethodModel extends Model
{
    protected static $table = 'deliverymethods';

    protected static $primary = 'DeliveryMethodID';

    public $deliveryMethodID;

    public $deliveryMethodName;

    public $lastEditedBy;
}