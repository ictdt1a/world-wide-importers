<?php
include_once '../lib/Model.php';

class InvoiceLineModel extends Model
{
    protected static $table = 'invoicelines';

    protected static $primary = 'InvoiceLineID';

    protected static $validEnabled = false;

    public $invoiceLineID;

    public $invoiceID;

    public $stockItemID;

    public $description;

    public $packageTypeID;

    public $quantity;

    public $unitPrice;

    public $taxRate;

    public $taxAmount;

    public $lineProfit;

    public $extendedPrice;

    public $lastEditedBy;

    public $lastEditedWhen;
}