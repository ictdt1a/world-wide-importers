<?php
include_once '../lib/Model.php';

class InvoiceModel extends Model
{
    protected static $table = 'invoices';

    protected static $primary = 'InvoiceID';

    protected static $validEnabled = false;

    public $invoiceID;

    public $customerID;

    public $billToCustomerID;

    public $orderID;

    public $deliveryMethodID;

    public $contactPersonID;

    public $accountsPersonID;

    public $salespersonPersonID;

    public $packedByPersonID;

    public $invoiceDate;

    public $isCreditNote;

    public $totalDryItems;

    public $totalChillerItems;

    public $lastEditedBy;

    public $lastEditedWhen;
}