<?php
include_once '../lib/Model.php';

class MediaModel extends Model
{

    protected static $table = 'media';

    protected static $primary = 'MediaID';

    public $mediaID;

    public $stockMediaID;

    public $mediaTypeID;

    public $location;

    public $comments;

    public $lastEditedBy;

}