<?php
include_once '../lib/Model.php';

class OrderLineModel extends Model
{
    protected static $table = 'orderlines';

    protected static $primary = 'OrderLineID';

    protected static $validEnabled = false;

    public $orderLineID;

    public $orderID;

    public $stockItemID;

    public $description;

    public $packageTypeID;

    public $quantity;

    public $unitPrice;

    public $taxRate;

    public $pickedQuantity;

    public $pickingCompletedWhen;

    public $lastEditedBy;

    public $lastEditedWhen;

    /**
     * @param $orderID
     * @return OrderLineModel[]
     * @throws Exception
     */
    public function getByOrderID($orderID)
    {
        return OrderLineModel::get(['orderID' => $orderID]);
    }
}