<?php
include_once '../lib/Model.php';

class OrderModel extends Model
{
    protected static $table = 'orders';

    protected static $primary = 'OrderID';

    protected static $validEnabled = false;

    public $orderID;

    public $customerID;

    public $salespersonPersonID;

    public $contactPersonID;

    public $orderDate;

    public $expectedDeliveryDate;

    public $isUndersupplyBackordered;

    public $lastEditedBy;

    public $lastEditedWhen;
}