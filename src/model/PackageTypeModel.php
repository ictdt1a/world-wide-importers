<?php
include_once '../lib/Model.php';

class PackageTypeModel extends Model
{
    protected static $table = 'packagetypes';

    protected static $primary = 'PackageTypeID';

    public $packageTypeID;

    public $packageTypeName;

    public $lastEditedBy;
}