<?php
include_once '../lib/Model.php';

class PeopleModel extends Model
{
    protected static $table = 'people';

    protected static $primary = 'personID';

    public $personID;

    public $fullName;

    public $preferredName;

    public $searchName;

    public $isPermittedToLogon;

    public $logonName;

    public $isExternalLogonProvider;

    public $isSystemUser;

    public $isEmployee;

    public $isSalesPerson;

    public $userPreferences;

    public $phoneNumber;

    public $faxNumber;

    public $emailAddress;

    public $photo;

    public $customFields;

    public $otherLanguages;

    /**
     * Validate if password is correct for this email/user.
     * Will also check if given email/user is allowed to logon.
     *
     * @param string $email
     * @param string $password
     * @return bool
     */
    public static function validatePassword($email, $password)
    {
        $qr = new QueryBuilder();
        $result = $qr->select(['p.HashedPassword', 'p.IsPermittedToLogon'])
            ->from(static::$table, 'p')
            ->where(["p.LogonName", "=", $email])
            ->retrieve();

        if ($result && $result[0]['IsPermittedToLogon'] === 1) {
            return password_verify($password, $result[0]['HashedPassword']);
        }

        return false;
    }

    /**
     * @param string $email
     * @return Model
     * @throws Exception
     */
    public static function getByEmail($email)
    {
        return static::getOne(['emailAddress' => $email]);
    }
}