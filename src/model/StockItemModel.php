<?php
include_once '../lib/Model.php';

class StockItemModel extends Model
{
    protected static $table = 'stockitems';

    protected static $primary = 'StockItemID';

    public $stockItemID;

    public $stockItemName;

    public $supplierID;

    public $colorID;

    public $unitPackageID;

    public $outerPackageID;

    public $brand;

    public $size;

    public $leadTimeDays;

    public $quantityPerOuter;

    public $isChillerStock;

    public $taxRate;

    public $unitPrice;

    public $recommendedRetailPrice;

    public $typicalWeightPerUnit;

    public $marketingComments;

    public $searchDetails;

    public function getPriceWithTax()
    {
        return ($this->recommendedRetailPrice / 100) * (100 + $this->taxRate);
    }

}