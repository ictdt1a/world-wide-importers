<div class="uk-container uk-margin-large ">
    <h1>404: Pagina niet gevonden</h1>
    <p>Helaas kunnen we de pagina die u zoekt niet vinden. Dit kan omdat de pagina niet meer bestaat, of doordat er een typefout is gemaakt bij het invoeren van adres.</p>
    <p>U kunt via de zoekfunctie uw product vinden of de navigatie opnieuw proberen.</p>
    <form method="post" action="/zoeken">
        <div class="uk-inline uk-width-1-1">
            <div uk-form-custom="target: true">
                            <span class="uk-form-icon uk-form-icon-flip uk-icon" data-uk-icon="icon: search"><svg
                                    width="20"
                                    height="20"
                                    viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg"> <circle
                                        fill="none" stroke="#000" stroke-width="1.1" cx="9" cy="9" r="7"></circle> <path
                                        fill="none" stroke="#000" stroke-width="1.1"
                                        d="M14,14 L18,18 L14,14 Z"></path></svg></span>
                <input class="uk-input uk-width-1-1 search-fld" name="searchTerm" type="search" placeholder="Zoeken...">
            </div>
            <button type="submit" name="search" class="uk-button uk-button-default">Zoeken</button>
        </div>
    </form>
    <p>Bijvoorbeeld: "Chilly Chocolate"</p>
</div>