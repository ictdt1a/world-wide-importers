<div class="uk-container uk-margin-large" xmlns="http://www.w3.org/1999/html">
    <div class="uk-grid-large uk-child-1-1@s uk-float-right" data-uk-grid="">
        <a href="/winkelwagen/clear">
            <button class="uk-button uk-button-danger">Winkelwagen leegmaken</button>
        </a>
    </div>
    <?php foreach ($data['products'] as $key => $product) { ?>
        <div class="uk-grid-large uk-child-width@s " data-uk-grid="">
            <div class="uk-width-auto@m">
                <a href="/product/<?= $product[0]->stockItemID ?>" class="" title=""><img
                            src="https://placehold.jp/100x100.png" width="100" alt=""></a>
            </div>
            <div class="uk-width-expand@m">
                <a href="/product/<?= $product[0]->stockItemID ?>">
                    <h3><?= $product[0]->stockItemName ?></h3></a>
                <p><?= $product[0]->marketingComments ?></p>
            </div>
            <div class="uk-width-auto@m uk-flex uk-flex-middle">
                <div class="uk-width-expand">
                    <a href="/winkelwagen/minus/<?= $product[0]->stockItemID ?>">
                        <button class="uk-button uk-button-secondary uk-button-small"> -</button>
                    </a>
                    <input class="uk-input uk-form-width-xsmall" type="text" value="<?= $product['amount'] ?>" disabled>
                    <a href="/winkelwagen/add/<?= $product[0]->stockItemID ?>">
                        <button class="uk-button uk-button-secondary uk-button-small"> +</button>
                    </a>
                </div>
            </div>
            <div class="uk-width-auto@m uk-flex uk-flex-middle">
                <div>
                    <small>Prijs per stuk</small>
                    <br>
                    <h3 style="margin-top: 0;">
                        &euro; <?= money_format('%.2n', $product[0]->getPriceWithTax()) ?></h3>
                </div>
            </div>
            <div class="uk-width-auto@m uk-flex uk-flex-middle">
                <div>
                    <small>Subtotaal</small>
                    <br>
                    <h3 style="margin-top: 0;">
                        &euro; <?= money_format('%.2n', $product[0]->getPriceWithTax() * $product['amount']) ?></h3>
                </div>
            </div>
            <div class="uk-width-auto@m uk-flex uk-flex-middle">
                <a href="/winkelwagen/delete/<?= $product[0]->stockItemID ?>">
                    <button class="uk-button uk-button-danger uk-button-small"><span uk-icon="icon: close"></span>
                    </button>
                </a>
            </div>
        </div>
    <?php } ?>
    <hr>
    <div class="uk-grid uk-child-1-1@s" uk-grid="">
        <div class="uk-width-expand@m">

        </div>
        <div class="uk-width-1-3@m uk-text-right@m">
            <table class="uk-table">
                <tbody>
                <tr>
                    <td>Exclusief BTW:</td>
                    <td> &euro; <?= money_format('%.2n', $data['total']) ?></td>
                </tr>
                <tr>
                    <td><b>Totaal:</b></td>
                    <td><b> &euro; <?= money_format('%.2n', $data['totalBtw']) ?></b></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <hr>
    <a href="/bestellen">
        <button class="uk-button uk-button-primary uk-float-right">bestellen</button>
    </a>
</div>


