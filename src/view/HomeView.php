<div class="uk-position-relative  uk-visible-toggle uk-light slider" uk-slideshow="min-height: 300; max-height: 400">

    <ul class="uk-slideshow-items">
        <li>
            <img src="https://getuikit.com/docs/images/photo.jpg" alt="" uk-cover>
            <div class="uk-container-large uk-position-center ">
                <div class="uk-child-width-expand@s" uk-grid>
                    <div>
                        <h1>Homepage</h1>
                    </div>
                </div>

            </div>
        </li>
        <li>
            <img src="https://getuikit.com/docs/images/dark.jpg" alt="" uk-cover>
        </li>
        <li>
            <img src="https://getuikit.com/docs/images/light.jpg" alt="" uk-cover>
        </li>
    </ul>

    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
       uk-slideshow-item="previous"></a>
    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
       uk-slideshow-item="next"></a>

</div>

<div class="uk-container uk-margin-large ">
    <div class="uk-child-width-1-4@s" uk-grid>

        <?php
            foreach( $data['products'] as $product ) {


        ?>
        <div>
            <div class="uk-card uk-card-default">
                <div class="uk-card-media-top">
                    <img src="<?= $product['Location'] ? $product['Location'] : 'https://placehold.jp/300x200.png' ?>" alt="">
                </div>
                <div class="uk-card-body">
                    <h3 class="uk-card-title"><?= $product['StockItemName'] ?></h3>
                    <h2>&euro; <?= money_format('%.2n', ($product['RecommendedRetailPrice'] / 100) * ($product['TaxRate'] + 100)) ?></h2>
                    <a href="product/<?= $product['StockItemID'] ?>"><button class="uk-button uk-button-primary">Bekijken</button></a>
                </div>
            </div>
        </div>

        <?php } ?>

    </div>
</div>