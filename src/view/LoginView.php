<div class="uk-container uk-margin-large">
    <div uk-grid class="uk-margin-small">
        <div class="uk-width-1-1 uk-width-1-2@s">
            <form method="POST" class="uk-form-stacked">
                <?php if (!empty($data['errors']['error'])) { ?>
                    <p class="uk-text-danger"><?= $data['errors']['error'] ?></p>
                <?php } ?>
                <div>
                    <label class="uk-form-label">
                        E-mail
                        <?php if (!empty($data['errors']['email'])) { ?>
                            <span class="uk-text-danger"><?= $data['errors']['email'] ?></span>
                        <?php } ?>
                    </label>
                    <div class="uk-form-controls">
                        <input class="uk-input <?=(!empty($data['errors']['email']) ? 'uk-form-danger' : '')?>" type="email" placeholder="E-mail" name="email"/>
                    </div>
                </div>
                <div>
                    <label class="uk-form-label">
                        Wachtwoord
                        <?php if (!empty($data['errors']['password'])) { ?>
                            <span class="uk-text-danger"><?= $data['errors']['password'] ?></span>
                        <?php } ?>
                    </label>
                    <div class="uk-form-controls">
                        <input class="uk-input <?=(!empty($data['errors']['password']) ? 'uk-form-danger' : '')?>" type="password" placeholder="Wachtwoord" name="password"/>
                    </div>
                </div>
                <div class="uk-margin-small">
                    <button class="uk-button uk-button-primary" type="submit" name="login">Login</button>
                </div>
            </form>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s">
            <h3>Nieuw bij World Wide Importers?</h3>
            <p>Maak binnen 2 minuten een account aan.</p>
            <a class="uk-button uk-button-default" href="/registreren">Registreren</a>
        </div>
    </div>
</div>
