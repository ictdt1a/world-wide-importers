<div class="uk-container uk-margin-large">
    <div class="uk-grid uk-child-width-1-1@s" uk-grid>
        <div class="">
            <h2>Betaling voltooid</h2>
            <h4>Bestelling <b>#<?= $data['order']->orderID ?></b> word verwerkt.</h4>
        </div>
        <div class="">
            <div class="uk-alert-success" uk-alert>
                <a class="uk-alert-close" uk-close></a>
                <h3>Bestelling is doorgevoerd.</h3>
                <p>U zal het factuur <b>#<?= $data['invoice']->invoiceID ?></b> via de mail ontvangen. Deze e-mail kan mogelijk bij de spam folder terecht komen.</p>
            </div>
        </div>
    </div>
</div>
