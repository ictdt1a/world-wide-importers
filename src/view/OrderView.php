<form method="POST">
    <div class="uk-container uk-margin-large" xmlns="http://www.w3.org/1999/html">
        <div class="uk-grid-large uk-child-1-1@s ">
            <h2>Bestellen.</h2>
        </div>
        <div class="uk-grid uk-child-width-1-1@s">
            <div>
                <ul uk-accordion="multiple: true">
                    <li class="uk-open">
                        <a class="uk-accordion-title" href="#">Gegevens.
                            <small>controleren</small>
                        </a>
                        <div class="uk-accordion-content">
                            <div class="uk-grid uk-child-width-1-2@s" uk-grid>
                                <div>
                                    <h4>Factuur adres</h4>
                                    <ul class="uk-list">
                                        <li>Naam: <?= $user->fullName ?></li>
                                        <li>Adres: LoremIpsumstraat 5</li>
                                        <li>Plaats: LoremStad</li>
                                        <li>Telefoonnummer: <?= $user->phoneNumber ?></li>
                                    </ul>
                                </div>
                                <div>
                                    <h4>Aflever adres</h4>
                                    <ul class="uk-list">
                                        <li>Naam: <?= $user->fullName ?></li>
                                        <li>Adres: LoremIpsumstraat 5</li>
                                        <li>Plaats: LoremStad</li>
                                        <li>Telefoonnummer: <?= $user->phoneNumber ?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="uk-open">
                        <a class="uk-accordion-title" href="#">Producten.
                            <small>controleren</small>
                        </a>
                        <div class="uk-accordion-content">
                            <?php foreach ($data['products'] as $key => $product) { ?>
                                <div class="uk-grid-large uk-child-width@s " data-uk-grid="">
                                    <div class="uk-width-auto@m">
                                        <a href="/product/<?= $product[0]->stockItemID ?>"><img
                                                    src="https://placehold.jp/100x100.png" alt=""></a>
                                    </div>
                                    <div class="uk-width-expand@m">
                                        <a href="/product/<?= $product[0]->stockItemID ?>">
                                            <h3><?= $product[0]->stockItemName ?>
                                                <small> x <?= $product['amount'] ?></small>
                                            </h3>
                                        </a>
                                        <p><?= $product[0]->marketingComments ?></p>
                                    </div>
                                    <div class="uk-width-auto@m uk-flex uk-flex-middle">
                                        <h3>&euro; <?= money_format('%.2n',$product[0]->getPriceWithTax() * $product['amount']) ?></h3>
                                    </div>
                                </div>
                                <hr>
                            <?php } ?>
                            <div class="uk-grid uk-child-1-1@s" uk-grid="">
                                <div class="uk-width-expand@m"></div>
                                <div class="uk-width-1-3@m uk-text-right@m">
                                    <table class="uk-table">
                                        <tbody>
                                        <tr>
                                            <td>Exclusief BTW:</td>
                                            <td> &euro; <?= money_format('%.2n', $data['total']) ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Totaal:</b></td>
                                            <td><b> &euro; <?= money_format('%.2n', $data['totalBtw']) ?></b></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="uk-grid-large uk-child-1-1@s uk-float-right" data-uk-grid="">
            <button class="uk-button uk-button-primary">Betalen</button>
        </div>
    </div>
</form>
