<div class="uk-container uk-margin-large ">


    <div class="uk-child-width-1-2@s" uk-grid>
        <div>
            <div class="" uk-slideshow="animation: fade">
                <div uk-lightbox>
                    <ul class="uk-slideshow-items">
                        <?php if (empty($data['media'])) { ?>
                            <li>
                                <img src="https://placehold.jp/600x400.png" alt="" uk-cover>
                                <div class="uk-position-top-right uk-overlay">
                                    <a href="https://placehold.jp/600x400.png">
                                        <span class="expand-media" uk-icon="expand"></span>
                                    </a>
                                </div>
                            </li>
                            <?php
                        } else {
                            foreach ($data['media'] as $key => $mediaItem) { ?>
                                <?php if ($mediaItem['MediaTypeID'] == 1) {
                                    ?>
                                    <li>
                                        <img src="<?= $mediaItem['Location'] ?>" alt="" uk-cover>
                                        <div class="uk-position-top-right uk-overlay">
                                            <a href="<?= $mediaItem['Location'] ?>">
                                                <span class="expand-media" uk-icon="expand"></span>
                                            </a>
                                        </div>
                                    </li>
                                <?php } ?>
                                <?php if ($mediaItem['MediaTypeID'] == 2) { ?>
                                    <li>
                                        <iframe width="560" height="315"
                                                src="<?= preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","//www.youtube.com/embed/$1", $mediaItem['Location']); ?>"
                                                frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                    </li>
                                    <?php
                                }

                            }
                        }
                        ?>

                    </ul>
                </div>


                <div class="uk-position-small">
                    <ul class="uk-thumbnav">
                        <?php
                        $i = 0;
                        foreach ($data['media'] as $mediaItem) {

                            ?>
                            <li uk-slideshow-item="<?= $i ?>">
                                <a href="#">
                                    <?php if ($mediaItem['MediaTypeID'] == 1) { ?>
                                        <img src="<?= $mediaItem['Location'] ?>" width="100" alt="">
                                    <?php } else if ($mediaItem['MediaTypeID'] == 2) { ?>
                                        <img src="https://img.youtube.com/vi/<?= preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","$1", $mediaItem['Location']); ?>/default.jpg"
                                             width="100" alt="">
                                    <?php } ?>
                                </a>
                            </li>
                            <?php
                            $i++;
                        } ?>
                    </ul>
                </div>
            </div>
        </div>
        <div>
            <div class="row">
                <h2><?= $data['product']['StockItemName'] ?></h2>
                <h1><b>&euro; <?= money_format('%.2n', ($data['product']['RecommendedRetailPrice'] / 100) * ($data['product']['TaxRate'] + 100)) ?></b></h1>
            </div>
            <div class="row uk-margin">
                <a href="/winkelwagen/add/<?= $data['product']['StockItemID']; ?>">
                    <button class="uk-button uk-button-secondary"><span uk-icon="cart"></span>In winkelwagen</button>
                </a>
            </div>
            <ul uk-tab uk-switcher>
                <?= ($data['product']['MarketingComments'] ? '<li><a href="#">Productbeschrijving</a></li>' : "") ?>
                <li><a href="#">Product specificaties</a></li>
            </ul>
            <ul class="uk-switcher uk-margin">
                <?= ($data['product']['MarketingComments'] ? '<li> ' . $data['product']['MarketingComments'] . '</li>' : "") ?>
                <li>
                    <ul class="uk-list">
                        <?= ($data['product']['brand'] ? '<li><b>Merk: </b>' . $data['product']['brand'] . '</li>' : "") ?>
                        <?= ($data['product']['Colorname'] ? '<li><b>Kleur: </b>' . $data['product']['Colorname'] . '</li>' : "") ?>
                        <?= ($data['product']['size'] ? '<li><b>Afmetingen: </b>' . $data['product']['size'] . '</li>' : "") ?>
                        <?= ($data['product']['TypicalWeightPerUnit'] ? '<li><b>Gewicht: </b>' . $data['product']['TypicalWeightPerUnit'] . '</li>' : "") ?>
                        <?= ($data['product']['PackageTypeName'] ? '<li><b>Prijs per: </b>' . $data['product']['PackageTypeName'] . '</li>' : "") ?>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
</div>