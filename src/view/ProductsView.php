<div class="uk-container uk-margin-large">

    <?php if ( !empty ( $data['searchTerm'] ) )  { ?>
        <h2>U heeft gezocht op: "<?= $data['searchTerm'] ?>"</h2>
        <h3>Gevonden resultaten:</h3>
    <?php } ?>

    <?php if ( !empty ( $data['category'] ) ) { ?>
        <h2><?= $data['category']->stockGroupName ?></h2>
        <h3><?= $data['category']->stockGroupName ?> resultaten:</h3>
    <?php } ?>

    <?php foreach ( $data['products'] as $product ) { ?>
        <div class="uk-grid-large uk-child-width@s " data-uk-grid="">
            <div class="uk-width-auto@m">
                <a href="/product/<?= $product['StockItemID'] ?>" class="" title=""><img src="<?= $product['Location'] ? $product['Location'] : 'https://placehold.jp/300x200.png' ?>" width="300" alt=""></a>
            </div>
            <div class="uk-width-expand@m">
                <a href="/product/<?= $product['StockItemID'] ?>"><h3><?= $product['StockItemName'] ?></h3></a>
                <?= ($product['MarketingComments'] ? '<p> ' . $product['MarketingComments'] . '</p>' : "" ) ?>
            </div>

            <div class="uk-width-auto@m uk-flex uk-flex-middle">
                <h3>&euro; <?= money_format('%.2n', ($product['RecommendedRetailPrice'] / 100) * ($product['TaxRate'] + 100)) ?></h3>
            </div>
            <div class="uk-width-auto@m uk-flex uk-flex-middle">
                <a href="/product/<?= $product['StockItemID'] ?>">
                    <button class="uk-button uk-button-secondary uk-button-small"><span uk-icon="icon: eye"></span> Bekijken</button>
                </a>
            </div>
        </div>
    <?php } ?>

</div>
