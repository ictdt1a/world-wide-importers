<div class="uk-container uk-margin-large">
    <h1>Account informatie</h1>
    <div class="uk-grid-medium uk-child-width-expand@s " uk-grid>
        <div class="uk-width-1-2@s">
            <label class="uk-form-label" for="form-stacked-text">Roepnaam:</label>
            <div class="uk-form-controls">
                <p><?= $user->preferredName ?></p>
            </div>
        </div>
        <div class="uk-width-1-2@s">
            <label class="uk-form-label" for="form-stacked-text">Volledige naam:</label>
            <div class="uk-form-controls">
                <p><?= $user->fullName ?></p>
            </div>
        </div>
    </div>
    <div class="uk-grid-medium uk-child-width-expand@s " uk-grid>
        <div class="uk-width-1-2@s ">
            <label class="uk-form-label" for="form-stacked-text">Telefoonnummer:</label>
            <div class="uk-form-controls">
                <p><?= $user->phoneNumber ?></p>
            </div>
        </div>
        <div class="uk-width-1-2@s ">
            <label class="uk-form-label" for="form-stacked-text">E-mail:</label>
            <div class="uk-form-controls">
                <p><?= $user->emailAddress ?></p>
            </div>
        </div>
    </div>
    <div class="uk-margin-small">
        <a href="/loguit" class="uk-button uk-button-primary">Uitloggen</a>
    </div>
</div>
