<div class="uk-container uk-margin-large">
    <?php if($data['done']) { ?>
    <h1>Bedankt</h1>
        <div uk-grid class="uk-margin-small">
            <div class="uk-width-1-1 uk-width-1-2@s">
                <p>Bedankt voor het registreren bij Wide World Importers, in uw mailbox vindt u een confirmatie e-mail van uw registratie bij ons!</p>
            </div>
            <div class="uk-width-1-1 uk-width-1-2@s">
                <h3>Login met uw account bij Wide World Importers</h3>
                <p>Klik dan hieronder om direct in te loggen.</p>
                <a class="uk-button uk-button-default" href="/login">Login</a>
            </div>
        </div>
    <?php } else { ?>
    <h1>Registreren</h1>
    <div uk-grid class="uk-margin-small">
        <div class="uk-width-1-1 uk-width-1-2@s">
            <form method="POST">
                    <?php if (!empty($data['errors']['generic'])) { ?>
                        <p class="uk-text-danger"><?= $data['errors']['generic'] ?></p>
                    <?php } ?>
                <?php if (!empty($data['errors']['mismatch'])) { ?>
                    <p class="uk-text-danger"><?= $data['errors']['mismatch'] ?></p>
                <?php } ?>
                <div class="uk-grid-medium uk-child-width-expand@s " uk-grid>
                    <div class="uk-width-1-3@s ">
                        <label class="uk-form-label" for="form-stacked-text">Voornaam: *</label>
                        <div class="uk-form-controls">
                            <input class="uk-input" id="form-stacked-text" type="text" name="firstname" placeholder="Voornaam" value="<?=$_POST["firstname"]?>">
                        </div>
                    </div>
                    <div class="uk-width-1-3@s ">
                        <label class="uk-form-label" for="form-stacked-text">Tussenvoegsel</label>
                        <div class="uk-form-controls">
                            <input class="uk-input" type="text" name="middlename" placeholder="Tussenvoegsel" value="<?=$_POST["middlename"]?>">
                        </div>
                    </div>
                    <div class="uk-width-1-3@s ">
                        <label class="uk-form-label" for="form-stacked-text">Achternaam: *</label>
                        <div class="uk-form-controls">
                            <input class="uk-input" type="text" name="lastname" placeholder="Achternaam" value="<?=$_POST["lastname"]?>">
                        </div>
                    </div>
                </div>
                <div class="uk-grid-medium uk-child-width-expand@s " uk-grid>
                    <div class="uk-width-1-2@s ">
                        <label class="uk-form-label" for="form-stacked-text">E-mailadres: *</label>
                        <div class="uk-form-controls">
                            <input class="uk-input" type="email" name="email" placeholder="E-mailadres" value="<?=$_POST["email"]?>">
                        </div>
                    </div>
                    <div class="uk-width-1-2@s ">
                        <label class="uk-form-label" for="form-stacked-text">Telefoonnummer: *</label>
                        <div class="uk-form-controls">
                            <input class="uk-input" type="tel" name="phonenumber" placeholder="Telefoonnummer" value="<?=$_POST["phonenumber"]?>">
                        </div>
                    </div>
                </div>
                <div class="uk-grid-medium uk-child-width-expand@s " uk-grid>
                    <div class="uk-width-1-2@s ">
                        <label class="uk-form-label" for="form-stacked-text">Wachtwoord: *</label>
                        <div class="uk-form-controls">
                            <input class="uk-input" type="password" name="password" placeholder="Wachtwoord">
                        </div>
                    </div>
                    <div class="uk-width-1-2@s ">
                        <label class="uk-form-label" for="form-stacked-text">Herhaal wachtwoord: *</label>
                        <div class="uk-form-controls">
                            <input class="uk-input" type="password" name="r_password" placeholder="Herhaal wachtwoord">
                        </div>
                    </div>
                </div>
                <div class="uk-margin-small">
                    <button class="uk-button uk-button-primary">Verzenden</button>
                </div>
            </form>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@s">
            <h3>Al een account bij World Wide Importers?</h3>
            <p>Klik dan hieronder om direct in te loggen.</p>
            <a class="uk-button uk-button-default" href="/login">Login</a>
        </div>
    </div>
    <?php } ?>
</div>
