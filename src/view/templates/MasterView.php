<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $this->title; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- UIkit CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.24/css/uikit.min.css"/>
        <link rel="stylesheet" href="/css/style.css"/>
        <!-- UIkit JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.24/js/uikit.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.24/js/uikit-icons.min.js"></script>
    </head>
    <body>
        <?php echo Renderer::render("./templates/navbar-template"); ?>
        <?php echo Renderer::render("./templates/notification-template"); ?>
        <?php echo $data; ?>
        <?php echo Renderer::render("./templates/footer-template"); ?>
    </body>
</html>