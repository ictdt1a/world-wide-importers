<?php echo Renderer::render("./templates/mail/mail-header"); ?>

<h1>Bestelling <b>#<?= $data['order']->orderID ?></b> voltooid</h1>
<p>Uw bestelde producten:</p>
<table>
    <tr>
        <th>Naam</th>
        <th>Prijs</th>
        <th>Hoeveelheid</th>
        <th>Subtotaal</th>
    </tr>
    <?php foreach ($data['products'] as $product) { ?>
        <tr>
            <td><?= $product[0]->stockItemName ?></td>
            <td>&euro; <?= $product[0]->unitPrice ?></td>
            <td><?= $product['amount'] ?></td>
            <td>&euro; <?= money_format('%.2n', $product[0]->getPriceWithTax() * $product['amount']) ?>  </td>
        </tr>
    <?php } ?>
</table>

<?php echo Renderer::render("./templates/mail/mail-footer"); ?>