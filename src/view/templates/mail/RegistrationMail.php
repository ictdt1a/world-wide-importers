<?php echo Renderer::render("./templates/mail/mail-header"); ?>

<h1>Beste <?= $data['person']->preferredName ?>,</h1>
<p>Uw Wide World Importers account is succesvol aangemaakt. Welkom!</p>

<p>Hieronder staan je accountgegevens, bewaar deze goed:</p>

<p><b class="uppercase">Gebruiker ID</b><br>#<?= $data['person']->personID ?></p>
<p><b class="uppercase">Gebruikersnaam</b><br><?= $data['person']->emailAddress ?></p>
<p><b class="uppercase">Wachtwoord</b><br>******</p>

<p><b>Let op</b>: uw inloggegevens zijn strik persoonlijk. Geeft deze dus <b>niet</b> door aan derden!</p>

<?php echo Renderer::render("./templates/mail/mail-footer"); ?>