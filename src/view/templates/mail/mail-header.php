<style type="text/css">
    body {
        font-family: Arial, sans-serif;
        font-size: 14px;
    }

    .uppercase {
        text-transform: uppercase;
        font-size: 10px;
    }

    table {
        border-collapse: collapse;
        border-spacing: 0;
    }

    table td {
        font-family: Arial, sans-serif;
        font-size: 14px;
        padding: 10px 5px;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        word-break: normal;
        border-color: black;
    }

    table th {
        font-family: Arial, sans-serif;
        font-size: 14px;
        font-weight: normal;
        padding: 10px 5px;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        word-break: normal;
        border-color: black;
    }

    table th {
        font-weight: bold;
        background-color: #efefef;
        border-color: #000000;
        text-align: left;
        vertical-align: top
    }

    table td {
        text-align: left;
        vertical-align: top
    }
</style>