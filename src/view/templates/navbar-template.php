<?php
//TODO find alternative for this.
include_once '../src/model/CategoryModel.php';
$categories = CategoryModel::get();

$pages = [
    ["page" => "Inloggen", "link" => "/login", "icon" => "user"],
    ["page" => "Winkelwagen", "link" => "/winkelwagen", "icon" => "cart"],
];

if (!is_null($user)) {
    $pages[0] = [
        "page" => $user->fullName,
        "link" => "/profiel",
        "icon" => "user"
    ];
}

?>

<div class="uk-container navbar uk-container-expand uk-visible@l">
    <div class="uk-grid uk-grid-medium uk-flex uk-flex-middle" data-uk-grid="">
        <div class="uk-width-auto uk-first-column">
            <a href="/" class="uk-logo" title=""><img src="/img/wwi_logo.svg" alt=""></a>
        </div>
        <div class="uk-width-expand">
            <form method="post" action="/zoeken">
                <div class="uk-inline uk-width-1-1">
                    <div uk-form-custom="target: true">
                            <span class="uk-form-icon uk-form-icon-flip uk-icon" data-uk-icon="icon: search"><svg
                                        width="20"
                                        height="20"
                                        viewBox="0 0 20 20"
                                        xmlns="http://www.w3.org/2000/svg"> <circle
                                            fill="none" stroke="#000" stroke-width="1.1" cx="9" cy="9" r="7"></circle> <path
                                            fill="none" stroke="#000" stroke-width="1.1"
                                            d="M14,14 L18,18 L14,14 Z"></path></svg></span>
                        <input class="uk-input uk-width-1-1 search-fld" name="searchTerm" type="search" placeholder="Zoeken...">
                    </div>
                    <button type="submit" name="search" class="uk-button uk-button-default">Zoeken</button>
                </div>
            </form>
        </div>
        <div class="uk-width-expand">
            <ul class="uk-subnav uk-float-right" data-uk-margin="">
                <?php foreach ($pages as $page) { ?>
                    <li class="uk-visible@s uk-first-column"><a href="<?= $page['link'] ?>"><span
                                    class="uk-margin-small-right"
                                    uk-icon="icon: <?= $page['icon'] ?>"></span> <?= $page['page'] ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>


<nav class="uk-navbar-container sub-navbar uk-visible@l" uk-navbar>
    <div class="uk-navbar-left">
        <ul class="uk-navbar-nav">
                <li>
                    <a href="#">Categorieën</a>
                    <div class="uk-navbar-dropdown uk-navbar-dropdown-width-2">
                        <div class="uk-navbar-dropdown-grid uk-child-width-1-1" uk-grid>
                            <div>
                                <ul class="uk-nav uk-navbar-dropdown-nav">
                                    <li class="uk-nav-header">Categorieën</li>
                                    <?php foreach ($categories as $category) { ?>
                                        <li><a href="/producten/categorie/<?=$category->stockGroupID?>"><?= $category->stockGroupName ?></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
        </ul>
    </div>
    <div class="uk-navbar-right">
        <ul class="uk-navbar-nav">
            <li><a href="#">Klantenservice</a></li>
        </ul>
    </div>
</nav>


<div class="uk-container navbar uk-container-expand uk-hidden@l">


    <div class="uk-child-width-1-2 " uk-grid>
        <div>
            <a href="/" class="uk-logo" title=""><img src="/img/wwi_logo.svg" alt=""></a>
        </div>
        <div class="">
            <a uk-navbar-toggle-icon="" href="#offcanvas-nav" uk-toggle="target: #offcanvas-nav"
               class="uk-float-right uk-navbar-toggle uk-float-left uk-navbar-toggle-icon uk-icon">
                <svg width="10" height="20" viewBox="0 0 10 20" xmlns="http://www.w3.org/2000/svg">
                    <rect y="9" width="10" height="2"></rect>
                    <rect y="3" width="10" height="2"></rect>
                    <rect y="15" width="10" height="2"></rect>
                </svg>
            </a>
        </div>
    </div>

    <div class="uk-child-width-1-1 " uk-grid>
        <div>
            <form method="post" action="/zoeken">
                <div class="uk-inline uk-width-1-1">
                    <div uk-form-custom="target: true">
                            <span class="uk-form-icon uk-form-icon-flip uk-icon" data-uk-icon="icon: search"><svg
                                        width="20"
                                        height="20"
                                        viewBox="0 0 20 20"
                                        xmlns="http://www.w3.org/2000/svg"> <circle
                                            fill="none" stroke="#000" stroke-width="1.1" cx="9" cy="9" r="7"></circle> <path
                                            fill="none" stroke="#000" stroke-width="1.1"
                                            d="M14,14 L18,18 L14,14 Z"></path></svg></span>
                        <input class="uk-input uk-width-1-1 search-fld" name="searchTerm" type="search" placeholder="Zoeken...">
                    </div>
                    <button type="submit" name="search" class="uk-button uk-button-default">Zoeken</button>
                </div>
            </form>
        </div>
    </div>

    <div id="offcanvas-nav" uk-offcanvas="flip: true">
        <div class="uk-offcanvas-bar">

            <ul class="uk-nav uk-nav-default">
                <li class="uk-nav-header">WWI</li>
                <?php
                foreach ($pages as $page) {
                    ?>
                    <li><a href="<?= $page['link'] ?>"><span class="uk-margin-small-right"
                                                             uk-icon="icon: <?= $page['icon'] ?>"></span> <?= $page['page'] ?>
                        </a></li>
                <?php } ?>
            </ul>

            <hr>

            <div class="uk-width-1-2@s uk-width-2-5@m">
                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                    <li class="uk-parent">
                        <a href="#">Categorieën</a>
                        <ul class="uk-nav-sub">
                        <?php foreach ($categories as $category) { ?>
                            <li><a href="/producten/categorie/<?=$category->stockGroupID?>"><?= $category->stockGroupName ?></a></li>
                        <?php } ?>
                        </ul>
                    </li>
                </ul>
            </div>

        </div>
    </div>
</div>