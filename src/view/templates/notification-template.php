<?php

    if ( $_SESSION['notification'] ) { ?>


        <?php
            if ( $_SESSION['notification']['status'] == 'warning' ) {
        ?>
        <div class="uk-container uk-margin-large">
            <div uk-alert class="uk-alert-warning">
                <a class="uk-alert-close" uk-close></a>
                <h3><?= $_SESSION['notification']['title'] ?></h3>
                <p><?= $_SESSION['notification']['message']?></p>
            </div>
        </div>
        <?php } ?>


        <?php if ( $_SESSION['notification']['status'] == 'general' ) { ?>
            <div class="uk-container uk-margin-large">
                <div uk-alert class="uk-alert-primary">
                    <a class="uk-alert-close" uk-close></a>
                    <h3><?= $_SESSION['notification']['title'] ?></h3>
                    <p><?= $_SESSION['notification']['message']?></p>
                </div>
            </div>
        <?php } ?>

        <?php if ( $_SESSION['notification']['status'] == 'success' ) { ?>
        <div class="uk-container uk-margin-large">
            <div uk-alert class="uk-alert-success">
                <a class="uk-alert-close" uk-close></a>
                <h3><?= $_SESSION['notification']['title'] ?></h3>
                <p><?= $_SESSION['notification']['message']?></p>
            </div>
        </div>
        <?php } ?>

        <?php if ( $_SESSION['notification']['status'] == 'danger' ) { ?>
        <div class="uk-container uk-margin-large">
            <div uk-alert class="uk-alert-danger">
                <a class="uk-alert-close" uk-close></a>
                <h3><?= $_SESSION['notification']['title'] ?></h3>
                <p><?= $_SESSION['notification']['message']?></p>
            </div>
        </div>
        <?php } ?>


<?php }

    // unset displayed notification
    unset( $_SESSION['notification']);

?>

